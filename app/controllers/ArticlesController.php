<?php
namespace app\controllers;

use app\helpers\Url;
use Yii;
use app\entities\Article;
use yii\data\Pagination;

class ArticlesController extends BaseController
{
    /**
     * @return string
     */
    public function actionIndex($year = '', $month='', $subject = '')
    {
        $query = Article::find()->last()->published();

        if($subject){
            $query->subject($subject);
        }

        if($year){
            $query->year($year);
        }

        if($month){
            $query->month($month);
        }

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 5;

        $articles = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', compact('articles', 'pages'));
    }


    /**
     * @param integer $id
     * @param string $slug
     * @return string
     */
    public function actionView($id, $slug = null)
    {
        /* @var $article \app\entities\Article */
        $article = Article::findOne($id);
        $this->ensureExists($article);

        if ($article->slug !== $slug) {
            return $this->redirect(Url::entity($article));
        }

        return $this->render('view', compact('article'));
    }
}

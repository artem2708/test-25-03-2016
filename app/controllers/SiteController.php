<?php
namespace app\controllers;

use Yii;

/**
 * Class SiteController
 * @author Artem Mironov <mironov2708@gmail.com>
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'info' => [
                'class' => 'yii\web\ViewAction',
                'viewPrefix' => '/info'
            ],
        ];
    }
}

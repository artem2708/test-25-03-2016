<?php
namespace app\rbac;

/**
 * @author Artem Mironov <mironov2708@gmail.com>
 */
class NotOwnerRule extends OwnerRule
{
    /**
     * @var boolean
     */
    public $inverse = true;
}
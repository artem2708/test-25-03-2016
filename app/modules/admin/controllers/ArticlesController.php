<?php
namespace app\modules\admin\controllers;

use app\entities\Article;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * @author Artem Mironov <mironov2708@gmail.com>
 */
class ArticlesController extends BaseController
{
    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Article::find()->orderBy('timeCreated DESC'),
        ]);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $article = new Article();
        $article->timePublished = date('Y-m-d H:i');

        if ($article->load(Yii::$app->request->post()) && $article->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', compact('article'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $article = Article::findOr404($id);

        if ($article->load(Yii::$app->request->post()) && $article->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', compact('article'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Article::findOr404($id)->delete();
        return $this->redirect('index');
    }
}
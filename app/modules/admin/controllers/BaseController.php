<?php
namespace app\modules\admin\controllers;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * @author Artem Mironov <mironov2708@gmail.com>
 */
abstract class BaseController extends \app\controllers\BaseController
{
    /**
     * @inheritdoc
     */
    public $layout = 'dashboard';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'controllers' => ['admin/auth'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
}
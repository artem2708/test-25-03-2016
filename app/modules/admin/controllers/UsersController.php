<?php
namespace app\modules\admin\controllers;

use app\models\ProfileForm;
use app\models\SignUpForm;
use app\models\UpdatePasswordForm;
use app\repositories\UserRepository;
use Yii;

/**
 * @author Artem Mironov <mironov2708@gmail.com>
 */
class UsersController extends BaseController
{
    /**
     * @var UserRepository
     */
    protected $users;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->users = new UserRepository();
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = $this->users->search();

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignUpForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->users->create($model->attributes, $model->password);
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('model'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $user = $this->users->getById($id);

        $profileModel = new ProfileForm();
        $profileModel->setAttributes($user->attributes);

        $updatePasswordModel = new UpdatePasswordForm();

        if (Yii::$app->request->isPost) {
            $shouldRedirect = true;

            if ($profileModel->load(Yii::$app->request->post()) && $profileModel->validate()) {
                $this->users->update($user, $profileModel->attributes);
            } else {
                $shouldRedirect = false;
            }

            if ($updatePasswordModel->load(Yii::$app->request->post()) && $updatePasswordModel->validate()) {
                $this->users->updatePassword($user, $updatePasswordModel->password);
            }

            if ($shouldRedirect) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', compact('user', 'profileModel', 'updatePasswordModel'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = $this->users->getById($id);
        $this->ensureExists($user);

        $this->users->delete($user);

        return $this->redirect(['index']);
    }
}
<?php
namespace app\modules\admin\controllers;

/**
 * Class DefaultController
 * @author Artem Mironov <mironov2708@gmail.com>
 */
class DefaultController extends BaseController
{
    public function actionIndex()
    {
        return $this->redirect(['articles/index']);
    }
}

<?php
/* @var $article  app\entities\Article */
/* @var $this     yii\web\View */

$this->title = $article->title;
$this->params['breadcrumbs'][] = ['label' => 'Статьи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<?= $this->render('_form', compact('article')) ?>
<?php
use app\entities\Subject;
use app\modules\admin\widgets\DateTimePicker;
use vova07\imperavi\Widget as Redactor;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $article app\entities\Article */
/* @var $this    yii\web\View */

$list = ArrayHelper::map(Subject::find()->all(), 'id', function (Subject $item) {
    return $item->name;
});
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'options' => ['enctype' => 'multipart/form-data'],
]) ?>

<div class="panel">
    <div class="panel-heading">Основная информация</div>
    <div class="panel-body">
        <?= $form->field($article, 'title')->textInput() ?>
        <?= $form->field($article, 'subject_id')->dropDownList($list); ?>
        <?= $form->field($article, 'timePublished')->widget(DateTimePicker::className(), [
            'clientOptions' => [
                'format' => 'YYYY-MM-DD HH:mm',
                'locale' => 'ru',
            ],
        ]) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">Контент</div>
    <div class="panel-body">
        <?= $form->field($article, 'content')->widget(Redactor::className()) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">SEO настройки</div>
    <div class="panel-body">
        <?= $form->field($article, 'slug')->textInput() ?>
        <?= $form->field($article, 'seoTitle')->textInput() ?>
        <?= $form->field($article, 'seoDescription')->textarea(['rows' => 2]) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>
</div>
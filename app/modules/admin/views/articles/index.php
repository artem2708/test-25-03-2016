<?php
use app\entities\Article;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $dataProvider yii\data\BaseDataProvider */
/* @var $this         yii\web\View */

$this->title = 'Статьи';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => $this->render('_grid'),
    'columns' => [
        [
            'attribute' => 'title',
            'format' => 'raw',
            'value' => function (Article $article) {
                return Html::a($article->title, ['update', 'id' => $article->id], ['class' => 'btn-block']);
            },
        ],
        [
            'attribute' => 'timePublished',
            'format' => 'raw',
            'contentOptions' => ['class' => 'text-center h4'],
            'headerOptions' => ['width' => 150, 'class' => 'text-center'],
            'value' => function (Article $article) {
                return Yii::$app->formatter->asDate($article->timePublished, 'dd MMM HH:mm');
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['class' => 'text-center'],
            'headerOptions' => ['width' => 70],
            'template' => '{update} {delete}',
        ],
    ],
]); ?>

<?php
use otsec\yii2\bootstrap\ActiveForm;
use otsec\yii2\fladmin\FlAdmin;
use yii\helpers\Html;

/* @var $user app\entities\User */
/* @var $profileModel app\models\ProfileForm */
/* @var $updatePasswordModel app\models\UpdatePasswordForm */
/* @var $this yii\web\View */

$this->title = 'Редактирование пользователя: ' . ' ' . $user->name;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<div class="user-update">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
    ]) ?>

    <?= FlAdmin::beginPanel('Основная информания') ?>
        <?= $form->field($profileModel, 'name')->textInput(['maxlength' => 100]) ?>
    <?= FlAdmin::endPanel() ?>

    <?= FlAdmin::beginPanel('Сменить пароль') ?>
        <?= $form->field($updatePasswordModel, 'password')->passwordInput() ?>
        <?= $form->field($updatePasswordModel, 'passwordRepeat')->passwordInput() ?>
    <?= FlAdmin::endPanel() ?>

    <?= FlAdmin::beginPanel() ?>
        <?= $form->beginWrapper() ?>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Вернуться назад', ['index'], ['class' => 'btn btn-default']) ?>
        <?= $form->endWrapper() ?>
    <?= FlAdmin::endPanel() ?>

    <?php $form->end() ?>
</div>

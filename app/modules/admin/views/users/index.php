<?php
use yii\grid\GridView;

/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $this         yii\web\View */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => $this->render('_grid'),
    'columns' => [
        [
            'attribute' => 'id',
            'contentOptions' => ['class' => 'text-center'],
            'headerOptions' => ['width' => 70],
        ],
        [
            'attribute' => 'email',
            'headerOptions' => ['width' => 300],
        ],
        'name',
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['class' => 'text-center'],
            'headerOptions' => ['width' => 70],
            'template' => '{update} {delete}',
        ],
    ],
]); ?>

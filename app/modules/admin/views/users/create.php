<?php
use otsec\yii2\bootstrap\ActiveForm;
use otsec\yii2\fladmin\FlAdmin;
use yii\helpers\Html;

/* @var $user  app\entities\User */
/* @var $model app\models\SignUpForm */
/* @var $this  yii\web\View */

$this->title = 'Новый пользователь';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Добавление';
?>

<div class="user-create">
    <?php $form = ActiveForm::begin(); ?>

    <?= FlAdmin::beginPanel('Новый пользователь') ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => 100]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => 100]) ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
    <?= FlAdmin::endPanel() ?>

    <?= FlAdmin::beginPanel() ?>
        <?= $form->beginWrapper() ?>
            <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Вернуться назад', ['index'], ['class' => 'btn btn-default']) ?>
        <?= $form->endWrapper() ?>
    <?= FlAdmin::endPanel() ?>

    <?php $form->end() ?>
</div>


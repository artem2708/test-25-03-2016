<?php
namespace app\modules\admin\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Mironov <mironov2708@gmail.com>
 */
class MomentAsset extends AssetBundle
{
    public $sourcePath = '@bower/moment';

    public $js = [
        'min/moment-with-locales.min.js',
    ];
}
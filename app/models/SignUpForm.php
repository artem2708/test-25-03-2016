<?php
namespace app\models;

use yii\base\Model;
use Yii;

/**
 * @author Artem Mironov <mironov2708@gmail.com>
 */
class SignUpForm extends Model
{
    public $name;
    public $email;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string', 'max' => 100],
            ['name', 'trim'],
            ['name', 'required'],

            ['email', 'string', 'max' => 100],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\entities\User', 'message' => 'Этот логин уже занят.'],

            ['password', 'string'],
            ['password', 'trim'],
            ['password', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Почта',
            'name' => 'Имя',
            'password' => 'Пароль',
        ];
    }
}

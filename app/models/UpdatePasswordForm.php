<?php
namespace app\models;

use app\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;

/**
 * @author Artem Mironov <mironov2708@gmail.com>
 */
class UpdatePasswordForm extends Model
{
    public $password;
    public $passwordRepeat;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'string'],
            ['password', 'trim'],
            ['password', 'required'],

            ['passwordRepeat', 'string'],
            ['passwordRepeat', 'trim'],
            ['passwordRepeat', 'required'],
            ['passwordRepeat', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Новый пароль',
            'passwordRepeat' => 'Повторите пароль',
        ];
    }
}

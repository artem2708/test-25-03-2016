<?php
namespace app\models;

use yii\base\Model;

/**
 * @author Artem Mironov <mironov2708@gmail.com>
 */
class ProfileForm extends Model
{
    /**
     * @var string
     */
    public $name;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string'],
            ['name', 'trim'],
            ['name', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
        ];
    }
}
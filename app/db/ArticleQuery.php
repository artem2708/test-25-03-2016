<?php
namespace app\db;

use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * @author Artem Mironov <mironov2708@gmail.com>
 */
class ArticleQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function last()
    {
        return $this->orderBy('timePublished DESC');
    }

    /**
     * @return static
     */
    public function popular()
    {
        return $this->orderBy('timePublished DESC');
    }

    /**
     * @return static
     */
    public function published()
    {
        return $this->andWhere('timePublished < :now', [
            ':now' => date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * @param $year
     *
     * @return $this
     */
    public function year($year)
    {
        return $this->andWhere('YEAR(timePublished) = '. $year);
    }
    /**
     * @param $month
     *
     * @return $this
     */
    public function month($month)
    {
        return $this->andWhere('MONTH(timePublished) = '. $month);
    }

    /**
     * @param $subject
     *
     * @return $this
     */
    public function subject($subject)
    {
        return $this->andWhere('subject_id ='.$subject);
    }

    /**
     * @param integer $id
     * @return static
     */
    public function except($id)
    {
        return $this->andWhere(['!=', 'id', $id]);
    }
}
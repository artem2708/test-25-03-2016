<?php
namespace app\db;

use app\entities\User;
use yii\db\ActiveQuery;

/**
 * @author Artem Mironov <mironov2708@gmail.com>
 */
class UserQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function active()
    {
        return $this->andWhere(['status' => User::STATUS_ACTIVE]);
    }
}

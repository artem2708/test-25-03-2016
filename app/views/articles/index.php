<?php
use app\helpers\Url;
use app\widgets\ArticleWidget;
use app\widgets\PaginatorWidget;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $articles \app\entities\Article[] */
/* @var $similar \app\entities\Article[] */

$this->title = 'Статьи';
$this->registerMetaTag(['name' => 'description', 'content' => 'Описание']);
?>

<div class="inner-page">
    <div class="inner-page__inner">
        <?= ArticleWidget::widget() ?>
        <div class="inner-page__content">
            <?php foreach ($articles as $article): ?>
                <a class="block-title" href="<?= Url::entity($article) ?>"><?= $article->title ?></a>
                <div class="typo-text"><?= Yii::$app->formatter->asDate($article->timePublished) ?>, Тема: <?= $article->subject ? $article->subject->name : 'нет категории' ?></div>
                <div class="typo-text">
                    <div class="truncate-text">
                        <?= StringHelper::truncate(strip_tags($article->content),256); ?>
                    </div>

                    <a  href="<?= Url::entity($article) ?>">Читать далее</a>
                </div>
            <?php endforeach ?>

            <div class="pagin">
                <?= PaginatorWidget::widget([
                    'pagination' => $pages,
                    'prevPageCssClass' => 'pagin__item pagin__prev',
                    'nextPageCssClass' => 'pagin__item pagin__next',
                    'activePageCssClass' => 'pagin__item pagin__current',
                    'options' => ['class' => 'pagin__inner'],

                ]); ?>
            </div>
        </div>
    </div>
</div>
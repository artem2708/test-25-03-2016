<?php
use app\helpers\Url;
use app\widgets\ArticleWidget;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $article \app\entities\Article */
/* @var $similar \app\entities\Article[] */

$this->title = $article->seoTitle ?: $article->title;
$this->title = $this->title . ' - Статьи';

if ($article->seoDescription) {
    $this->registerMetaTag(['name' => 'description', 'content' => $article->seoDescription]);
}
$this->registerMetaTag(['property' => 'og:title', 'content' => $article->title]);
$this->registerMetaTag(['property' => 'og:description', 'content' => StringHelper::truncate(strip_tags($article->content),256,'...')]);
?>

<div class="inner-page">
    <div class="inner-page__inner">
        <div class="inner-page__content">
            <div class="typo-text">
                <h1><?= Html::encode($article->title) ?></h1>
                <p><?= Yii::$app->formatter->asDate($article->timePublished) ?></p>
                <h2>Тема: "<?= $article->subject ? $article->subject->name : 'нет категории' ?>"</h2>
                <?= Yii::$app->formatter->asHtml($article->content) ?>
            </div>

            <br/>

            <a class="similar-articles__link" href="<?= Url::to(['articles/index']) ?>">Все статьи</a>
        </div>

        <?= ArticleWidget::widget() ?>
    </div>
</div>
<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

/* @var $this    yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:type" content="website">
    <meta name='yandex-verification' content='4c58ad1f8c1763f1' />

    <link rel="apple-touch-icon" href="<?= Url::to('@web/apple-touch-icon.png') ?>">
    <link rel="icon" href="<?= Url::to('@web/apple-touch-icon.png') ?>">
    <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/eecoeldgoahknjcceomcpoaopeohkamf">
    <link rel="chromium-extension" href="<?= Url::to('@web/files/musicmio.crx') ?>">
    <link rel="firefox-extension" href="<?= Url::to('@web/files/musicmio.xpi') ?>">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <!--[if lt IE 8]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
        your browser</a> to improve your experience.</p>
    <![endif]-->

    <header class="header">
        <span class="header__shadow header__shadow--left"><!-----></span>
        <span class="header__shadow-center"><!-----></span>
        <span class="header__shadow header__shadow--right"><!-----></span>

        <div class="header__shadow-wrap">
            <div class="header__inner">

                <nav class="header__nav">
                    <a href="<?= Url::to(['articles/index']) ?>" <?= strpos(Yii::$app->request->url, 'articles') || Yii::$app->request->url == '/' ? 'class="current"' : '' ?>>СТАТЬИ</a>
                </nav>
            </div>
        </div>
    </header>

    <?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

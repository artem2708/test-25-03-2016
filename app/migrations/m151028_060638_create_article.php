<?php

use yii\db\Migration;

class m151028_060638_create_article extends Migration
{
    public function up()
    {
        $this->createTable('article', [
            'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT',
            'title' => 'VARCHAR(255) NOT NULL',
            'slug' => 'VARCHAR(255) NOT NULL',
            'subject_id' => 'INT(11) UNSIGNED DEFAULT NULL',
            'content' => 'TEXT',
            'seoTitle' => 'VARCHAR(255)',
            'seoDescription' => 'VARCHAR(255)',
            'timeCreated' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'timePublished' => 'TIMESTAMP',
            'PRIMARY KEY (id)',
        ]);

    }

    public function down()
    {
        $this->dropTable('article');
    }
}

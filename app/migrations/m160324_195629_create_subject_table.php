<?php

use yii\db\Migration;

class m160324_195629_create_subject_table extends Migration
{
    public function up()
    {
        $this->createTable('subject', [
            'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT',
            'name' => 'VARCHAR(255) NOT NULL',
            'PRIMARY KEY (id)',
        ]);


        $this->addForeignKey('subject_fk', 'article', 'subject_id', 'subject', 'id');

        $this->batchInsert('subject', ['name'], [
            ['Достоевский'],
            ['Толстой'],
            ['Булгаков'],
            ['Гоголь'],
            ['Пушкин'],
            ['Чехов'],
        ]);

    }

    public function down()
    {
        $this->dropTable('subject');
    }
}

<?php
namespace app\entities;

use app\db\ActiveRecord;
use app\db\ArticleQuery;
use app\db\ImageQuery;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $subjectId
 * @property string  $title
 * @property string  $slug
 * @property string  $content
 * @property string  $seoTitle
 * @property string  $seoDescription
 * @property string  $timeCreated
 * @property string  $timePublished
 *
 * @property Subject   $subject
 *
 * @author Artem Mironov <mironov2708@gmail.com>
 * @mixin TimestampBehavior
 */
class Article extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @return ArticleQuery
     */
    public static function find()
    {
        return new ArticleQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
                'immutable' => true,
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['title', 'string'],
            ['title', 'trim'],
            ['title', 'required'],

            ['subject_id', 'safe'],

            ['slug', 'string'],
            ['slug', 'trim'],
            ['slug', 'filter', 'filter' => ['yii\helpers\Inflector', 'slug']],
            ['slug', 'required', 'enableClientValidation' => false],

            ['content', 'string'],
            ['content', 'trim'],
            ['content', 'required'],

            ['seoTitle', 'string'],
            ['seoTitle', 'trim'],
            ['seoTitle', 'default'],

            ['seoDescription', 'string'],
            ['seoDescription', 'trim'],
            ['seoDescription', 'default'],

            ['timePublished', 'string'],
            ['timePublished', 'trim'],
            ['timePublished', 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'categoryId' => 'Категория',
            'title' => 'Название',
            'content' => 'Текст статьи',
            'slug' => 'Псевдоним URL',
            'seoTitle' => 'Тег TITLE',
            'seoDescription' => 'Тег META Description',
            'timePublished' => 'Время публикации',
            'subject_id' => 'Тема',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject(){

        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

}
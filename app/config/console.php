<?php
return [
    'id' => 'musicmio-console',
    'controllerNamespace' => 'app\console',

    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

    ],
];

<?php
use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'basePath' => dirname(dirname(__DIR__)),
    'runtimePath' => dirname(dirname(__DIR__)) . '/runtime',
    'viewPath' => dirname(__DIR__) . '/views',

    'aliases' => [
        '@app' => dirname(__DIR__),
    ],

    'bootstrap' => ['log'],
    'language' => 'ru-RU',

    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['admin', 'user'],
            'itemFile' => '@app/rbac/data/items.php',
            'assignmentFile' => '@app/rbac/data/assignments.php',
            'ruleFile' => '@app/rbac/data/rules.php',
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],

        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\entities\User',
            'enableAutoLogin' => true,
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'articles/<slug:[\w-]+>-<id:[\d]+>' => 'articles/view',
                'articles/<id:[\d]+>' => 'articles/view',


                'admin' => 'admin/default/index',
                'admin/<controller:\w+>' => 'admin/<controller>/index',

                '' => 'articles/index',
                '<controller:\w+>' => '<controller>/index',
            ],
        ],
    ],

    'params' => $params,
];

<?php
return [
    'id' => 'musicmio-web',

    'aliases' => [
        '@home' => '/site/index',
        '@page' => '/site/page',
    ],

    'components' => [
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
        ],

        'user' => [
            'loginUrl' => ['/admin/auth/login'],
        ],
    ],

    'modules' => [
        'admin' => 'app\modules\admin\Module',
    ],
];

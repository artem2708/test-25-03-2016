<?php
/**
 * Created by PhpStorm.
 * User: ART
 * Date: 28.10.15
 * Time: 12:32
 */
namespace app\widgets;

use yii\base\Widget;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\entities\Article;

class ArticleWidget extends Widget
{

    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        $dataYears = [];
        $dataSubjects = [];
        $years = (new Query())->from('article')->select('timePublished as date, MONTH(timePublished) as month,YEAR(timePublished) as year, COUNT(id) as count')->groupBy('month')->all();
        foreach($years as $item){
            if($item['year']){
                $dataYears[$item['year']][$item['month']] = ['count' => $item['count'], 'date'=> $item['date']];
            }
        }

        $subjects = (new Query())->from('article')->select('subject.id, subject.name, COUNT(article.id) as count')->leftJoin('subject','subject.id = article.subject_id')->groupBy('subject.name')->all();
        foreach($subjects as $item){
            if($item['name']){
                $dataSubjects[$item['name']] = ['count' => $item['count'], 'id'=>$item['id']];
            }
        }

        return $this->render('_articles', compact('dataYears', 'dataSubjects'));
    }
}

?>

<?php
use app\helpers\Url;

/* @var $last app\entities\Article[] */
/* @var $popular app\entities\Article[] */
?>

<div class="aside-panel">
    <div class="aside-block">
        <h3 class="aside-block__title">Годы</h3>
        <?php if (isset($dataYears)) foreach ($dataYears as $year => $article): ?>
            <p>
                <a href="<?= Url::to(['articles/index','year'=>$year ]) ?>"><?= $year ?></a>
            </p>
            <?php foreach ($article as $month => $item): ?>
                <p>
                    <a href="<?= Url::to(['articles/index','month'=>$month, 'year' => $year]) ?>"><?= Yii::$app->formatter->asDate($item['date'],'LLLL').'('.$item['count'].')'?></a>
                </p>
            <?php endforeach ?>
        <?php endforeach ?>
    </div>

    <div class="aside-block">
        <h3 class="aside-block__title">Темы</h3>
        <?php if (isset($dataSubjects)) foreach ($dataSubjects as $title => $subject): ?>
            <p>
                <a href="<?= Url::to(['articles/index','subject'=>$subject['id']]) ?>"><?= $title.'('. $subject['count'].')' ?></a>
            </p>
        <?php endforeach ?>
    </div>
</div>

/**
 * Social sharing
 */
(function () {
    'use strict';

    var socials = {};

    socials.openPopup = function(url, width, height) {
        window.open(url, '', 'toolbar=0,status=0,width=' + width + ',height=' + height);
    };

    socials.vk = {
        share: function(pageUrl) {
            var url  = 'http://vkontakte.ru/share.php?';
            url += 'url=' + encodeURIComponent(pageUrl);

            // hot fix
            url += '&title=' + encodeURIComponent($('meta[property="og:title"]').attr('content'));
            url += '&description=' + encodeURIComponent($('meta[property="og:description"]').attr('content'));
            url += '&image=' + encodeURIComponent($('meta[property="og:image"]').attr('content'));

            socials.openPopup(url, 550, 330);
        },
        count: function (pageUrl) {
            return new Promise(function (resolve, reject) {
                if ( ! window.VK) {
                    window.VK = {};
                }

                window.VK.Share = {
                    count: function(idx, number) {
                        resolve(number);
                    }
                };

                var url = 'https://vk.com/share.php?act=count&url=' + encodeURIComponent(pageUrl);
                $.getScript(url);
            });
        }
    };

    socials.facebook = {
        share: function(pageUrl) {
            var url  = 'http://www.facebook.com/sharer.php?s=100';
            url += '&p[url]=' + encodeURIComponent(pageUrl);
            socials.openPopup(url, 600, 500);
        },
        count: function (pageUrl) {
            return new Promise(function (resolve, reject) {
                var url = 'https://graph.facebook.com/?callback=?&ids=' + encodeURIComponent(pageUrl);
                $.getJSON(url, function(data){
                    resolve(data[pageUrl].shares);
                });
            });
        }
    };

    socials.twitter = {
        share: function(pageUrl, text) {
            var url  = 'http://twitter.com/share?';
            url += 'text=' + encodeURIComponent(text);
            url += '&url=' + encodeURIComponent(pageUrl);
            url += '&counturl=' + encodeURIComponent(pageUrl);
            socials.openPopup(url, 600, 305);
        },

        count: function (pageUrl) {
            return new Promise(function (resolve, reject) {
                var url = 'https://urls.api.twitter.com/1/urls/count.json?callback=?&url=' + encodeURIComponent(pageUrl);
                $.getJSON(url, function(data){
                    resolve(data.count);
                });
            });
        }
    };

    $('[data-share]').each(function () {
        var $this = $(this);

        var network = $this.data('share');
        var url = $this.data('share-url') || location.href;
        var text = $this.data('share-text') || document.title;

        var share = socials[network].share;
        var loadSharesCount = socials[network].count;

        $this.on('click', function (event) {
            event.preventDefault();
            share(url, text);
        });

        $this.find('.count').each(function () {
            var $counter = $(this);

            loadSharesCount(url).then(function (count) {
                count = parseInt(count) || 0;
                $counter.text(count);
            });
        });
    });
})();

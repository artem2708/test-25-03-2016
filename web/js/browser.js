
$('.js-browser-btn').each(function () {
    var $button = $(this);
    var browser = detectBrowser();

    switch (browser) {
        case 'firefox':
            $button.addClass('ff-btn');
            break;
        case 'opera':
            $button.addClass('opera-btn');
            break;
        case 'yabrowser':
            $button.addClass('yandex-btn');
            break;
        default:
            $button.addClass('chrome-btn');
    }
});

$('.js-install-btn').on('click', function (event) {
    event.preventDefault();

    var browser = detectBrowser();

    switch (browser) {
        case 'firefox':
            document.location.href = getExtensionUrl('firefox-extension');
            break;
        case 'chrome':
        case 'yabrowser':
            chrome.webstore.install(getExtensionUrl('chrome-webstore-item'));
            break;
        default:
            document.location.href = getExtensionUrl('chromium-extension');
    }
});

function detectBrowser () {
    var ua = navigator.userAgent;

    if (ua.search(/Firefox/) > -1) {
        return "firefox";
    }

    if (ua.search(/OPR/) > -1) {
        return "opera";
    }

    if (ua.search(/YaBrowser/) > -1) {
        return "yabrowser";
    }

    if (ua.search(/Chrome/) > -1) {
        return "chrome";
    }

    if (ua.search(/Safari/) > -1) {
        return "safari";
    }

    return null;
}

function getExtensionUrl (name) {
    return $('link[rel="' + name + '"]').attr('href');
}

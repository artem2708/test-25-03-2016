
$('.counter').counter();

$('.slidePanelContent').hide();
$('.slidePanelTitle').on('click', function(){
    $(this).closest('.slidePanelItem').find('.slidePanelContent').slideToggle();
});

$('.mainTabButton').on('click', function(){
    var idVal = $(this).attr('imageId');
    $('.mainTabButton, .mainTabImg').removeClass('current');
    $(this).addClass('current');
    $('#' + idVal).addClass('current');
});

$('.mainPageReviewSlider').carouFredSel({
    scroll : { fx : 'fade' },
    prev : '.mainPageReviewSliderArrowLeft',
    next: '.mainPageReviewSliderArrowRight'
});

$('.toggleTextBtn').on('click', function(){
    $(this).toggleClass('opened').closest('.toggleTextWrap').find('.hiddenText').slideToggle();
});